-- create student table
CREATE TABLE `dormitory`.`students` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `school` VARCHAR(255) NOT NULL,
  `year` int NOT NULL,
  `additional_email` VARCHAR(255) NULL,
  `phone_number` DOUBLE NULL,
  `roomId` int NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `microsoft_token` VARCHAR(2048) NOT NULL,
  `dormitoryId` INT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create offer table
CREATE TABLE `dormitory`.`offers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(1024) NOT NULL,
  `image_url` VARCHAR(255) NULL,
  `dormitoryId` INT NOT NULL,
  `studentId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create teacher table
CREATE TABLE `dormitory`.`teachers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(255) NOT NULL,
  `last_name` VARCHAR(255) NOT NULL,
  `phone_number` DOUBLE NOT NULL,
  `image_url` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `dormitoryId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create room table
CREATE TABLE `dormitory`.`rooms` (
  `id` int NOT NULL AUTO_INCREMENT,
  `number` INT NOT NULL,
  `gender` INT DEFAULT 1,
  `floor` INT NOT NULL,
  `student_count` INT NOT NULL,
  `image_url` VARCHAR(255) NOT NULL,
  `dormitoryId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create dormitory table
CREATE TABLE `dormitory`.`dormitories` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `address` VARCHAR(1024) NOT NULL,
  `capacity` INT NOT NULL,
  `floor` INT NOT NULL,
  `payment` DOUBLE NOT NULL,
  `image_url` VARCHAR(255) NOT NULL,
  `registration_status` VARCHAR(255) NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create information table
CREATE TABLE `dormitory`.`informations` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(1024) NOT NULL,
  `image_url` VARCHAR(255) NULL,
  `dormitoryId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create information table
CREATE TABLE `dormitory`.`reminders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `description` VARCHAR(1024) NOT NULL,
  `image_url` VARCHAR(255) NULL,
  `minus_points` INT NULL,
  `studentId` INT NOT NULL,
  `teacherId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create item table
CREATE TABLE `dormitory`.`items` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL,
  `count` INT NOT NULL,
  `roomId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create request table
CREATE TABLE `dormitory`.`requests` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(255) NULL,
  `phone_number` INT NULL, 
  `gender` INT DEFAULT 1 NOT NULL,
  `gpa` double NOT NULL,
  `gpa_desc` VARCHAR(255) NOT NULL,
  `additional_desc` VARCHAR(255) NULL,
  `score` INT NULL,
  `status` VARCHAR(255) NOT NULL,
  `studentId` INT NOT NULL,
  `dormitoryId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;

-- create affiliated schools table
CREATE TABLE `dormitory`.`affiliated_schools` (
  `id` int NOT NULL AUTO_INCREMENT,
  `affiliated_school` VARCHAR(255) NOT NULL,
  `score` INT NOT NULL,
  `dormitoryId` INT NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_unicode_ci;