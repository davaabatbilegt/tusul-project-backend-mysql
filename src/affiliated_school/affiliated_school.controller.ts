import { Controller, Get, Param, NotFoundException } from '@nestjs/common';
import { Affiliated_schoolService } from './affiliated_school.service';
import { Affiliated_school } from './entities/affiliated_school.entity';

@Controller('affiliated_schools')
export class Affiliated_schoolsController {
  constructor(
    private readonly affiliated_schoolService: Affiliated_schoolService,
  ) {}

  @Get()
  async findAll(): Promise<Affiliated_school[]> {
    return await this.affiliated_schoolService.findAll();
  }

  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Affiliated_school> {
    const affiliated_school = await this.affiliated_schoolService.findOneById(
      Number(id),
    );
    if (!affiliated_school) {
      throw new NotFoundException(`Affiliated_school with ID ${id} not found`);
    }
    return affiliated_school;
  }

  @Get(':affiliated_school/:dormitoryId')
  async findOneBySchoolDormitory(
    @Param() params: { affiliant_school: string; dormitory_id: number },
  ): Promise<Affiliated_school> {
    return this.affiliated_schoolService.findOneBySchoolDormitory(
      params.affiliant_school,
      params.dormitory_id,
    );
  }
}
