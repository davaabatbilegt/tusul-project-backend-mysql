import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Affiliated_schoolService } from './affiliated_school.service';
import { Affiliated_schoolsController } from './affiliated_school.controller';
import { Affiliated_school } from './entities/affiliated_school.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Affiliated_school])],
  providers: [Affiliated_schoolService],
  controllers: [Affiliated_schoolsController],
  exports: [Affiliated_schoolService],
})
export class Affiliated_schoolsModule {}
