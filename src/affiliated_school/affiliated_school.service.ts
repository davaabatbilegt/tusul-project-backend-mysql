import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Affiliated_school } from './entities/affiliated_school.entity';

@Injectable()
export class Affiliated_schoolService {
  constructor(
    @InjectRepository(Affiliated_school)
    private affiliated_schoolRepository: Repository<Affiliated_school>,
  ) {}

  findAll(): Promise<Affiliated_school[]> {
    return this.affiliated_schoolRepository.find();
  }

  findOneById(id: number): Promise<Affiliated_school | null> {
    return this.affiliated_schoolRepository.findOneBy({ id });
  }

  async findOneBySchoolDormitory(
    school: string,
    dormitory_id: number,
  ): Promise<Affiliated_school | null> {
    return await this.affiliated_schoolRepository.findOne({
      where: {
        affiliated_school: school,
        dormitoryId: dormitory_id,
      },
    });
  }
}
