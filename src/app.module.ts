import { Logger, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { DataSource } from 'typeorm';

import databaseConfig from './config/database-factory';
import { DormitoryModule } from './dormitory/dormitory.module';
import { AuthModule } from './auth/auth.module';
import { StudentModule } from './student/student.module';
import { RequestModule } from './request/request.module';
import { RoomModule } from './room/room.module';
import { Affiliated_schoolsModule } from './affiliated_school/affiliated_school.module';
import { InformationModule } from './information/information.module';
import { ReminderModule } from './reminder/reminder.module';
import { OfferModule } from './offer/offer.module';

const typeOrmConfig = {
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],
    }),
  ],
  inject: [ConfigService],
  useFactory: async (configService: ConfigService) =>
    configService.get('database'),
  dataSourceFactory: async (options) => new DataSource(options).initialize(),
};

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRootAsync(typeOrmConfig),
    AuthModule,
    DormitoryModule,
    StudentModule,
    RequestModule,
    RoomModule,
    Affiliated_schoolsModule,
    InformationModule,
    ReminderModule,
    OfferModule,
  ],
  controllers: [],
  providers: [Logger],
})
export class AppModule {}
