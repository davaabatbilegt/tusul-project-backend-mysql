import {
  Body,
  Controller,
  Post,
  HttpCode,
  HttpStatus,
  Request,
  Get,
} from '@nestjs/common';

import {
  AdminAccess,
  Public,
  StudentAccess,
  TeacherAccess,
} from './../decorators/guard.decorator';

import { SignInDto } from './dto/sign-in.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('login')
  signIn(@Body() signInDto: SignInDto) {
    return this.authService.signIn(signInDto);
  }

  @AdminAccess()
  @StudentAccess()
  @TeacherAccess()
  @Get('profile')
  getProfile(@Request() req) {
    return this.authService.getUser(req.user.id, req.user.type);
  }
}
