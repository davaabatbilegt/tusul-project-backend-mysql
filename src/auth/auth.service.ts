import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import { USER_TYPES } from './../utils/constants';
import { passwordMd5Encrypt } from './../utils/functions';

import { SignInDto } from './dto/sign-in.dto';
import { StudentService } from 'src/student/student.service';
import { TeacherService } from 'src/teacher/teacher.service';

@Injectable()
export class AuthService {
  constructor(
    private studentService: StudentService,
    private teacherService: TeacherService,
    private jwtService: JwtService,
  ) {}

  async signIn(signInDto: SignInDto): Promise<{ access_token: string }> {
    let user;
    switch (signInDto.type) {
      case USER_TYPES.STUDENT:
        user = await this.studentService.findOneByEmail(signInDto.email);
        break;
      case USER_TYPES.TEACHER:
        user = await this.teacherService.findOneByEmail(signInDto.email);
        break;
      default:
        throw new UnauthorizedException();
    }

    const password = await passwordMd5Encrypt(signInDto.password);
    if (user?.password !== password) {
      throw new UnauthorizedException();
    }
    const payload = {
      id: user.id,
      username: user.email,
      type: signInDto.type,
    };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }

  async getUser(userId: number, type: string): Promise<{ user }> {
    let user;
    switch (type) {
      case USER_TYPES.STUDENT:
        user = await this.studentService.findOneById(userId);
        break;
      case USER_TYPES.TEACHER:
        user = await this.teacherService.findOneById(userId);
        break;
      default:
        throw new UnauthorizedException();
    }
    return user;
  }
}
