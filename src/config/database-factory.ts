import { registerAs } from '@nestjs/config';
import { join } from 'path';
import { Affiliated_school } from 'src/affiliated_school/entities/affiliated_school.entity';
import { Dormitory } from 'src/dormitory/entities/dormitory.entity';
import { Information } from 'src/information/entities/information.entity';
import { Item } from 'src/item/entities/item.entity';
import { Offer } from 'src/offer/entities/offer.entity';
import { Reminder } from 'src/reminder/entities/reminder.entity';
import { Request } from 'src/request/entities/request.entity';
import { Room } from 'src/room/entities/room.entity';
import { Student } from 'src/student/entities/student.entity';
import { Teacher } from 'src/teacher/entities/teacher.entity';
import { MysqlConnectionOptions } from 'typeorm/driver/mysql/MysqlConnectionOptions';

export default registerAs(
  'database',
  (): MysqlConnectionOptions =>
    ({
      type: 'mysql',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT, 10),
      username: process.env.DATABASE_USER,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_DATABASE,
      entities: [
        Dormitory,
        Student,
        Teacher,
        Room,
        Request,
        Information,
        Reminder,
        Offer,
        Item,
        Affiliated_school,
      ],
      timezone: 'Z', // UTC_O
      synchronize: false,
      dropSchema: false,
      migrationsRun: false,
      autoLoadEntities: true,
      logging: ['warn', 'error'],
      logger: 'debug',
      migrations: [join(__dirname, 'src/migrations/*{.ts,.js}')],
    }) as MysqlConnectionOptions,
);
