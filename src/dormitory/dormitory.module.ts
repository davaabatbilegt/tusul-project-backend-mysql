import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Dormitory } from './entities/dormitory.entity';
import { DormitoryService } from './dormitory.service';
import { DormitoryController } from './dormitory.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Dormitory])],
  providers: [DormitoryService],
  controllers: [DormitoryController],
  exports: [DormitoryService],
})
export class DormitoryModule {}
