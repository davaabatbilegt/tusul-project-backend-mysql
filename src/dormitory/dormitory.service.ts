import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Dormitory } from './entities/dormitory.entity';
import { REGISTRATION_STATUSES } from 'src/utils/constants';

@Injectable()
export class DormitoryService {
  constructor(
    @InjectRepository(Dormitory)
    private dormitoryRepository: Repository<Dormitory>,
  ) {}

  findAll(): Promise<Dormitory[]> {
    return this.dormitoryRepository.find();
  }

  findOne(id: number): Promise<Dormitory | null> {
    return this.dormitoryRepository.findOneBy({ id });
  }

  async findOneByIdWithTeachers(id: number): Promise<Dormitory | null> {
    // Find the dormitory by its ID along with its associated teachers
    const dormitory = await this.dormitoryRepository
      .createQueryBuilder('dormitory')
      .leftJoinAndSelect('dormitory.teachers', 'teachers')
      .leftJoinAndSelect('dormitory.affiliated_schools', 'affiliated_schools')
      .where('dormitory.id = :id', { id })
      .getOne();
    if (!dormitory) {
      throw new NotFoundException(`Dormitory with ID ${id} not found`);
    }
    return dormitory;
  }

  async updateRegistrationStatus(
    id: number,
    registration_status: REGISTRATION_STATUSES,
  ): Promise<Dormitory | null> {
    try {
      const dormitory = await this.dormitoryRepository.findOneBy({ id });
      if (!dormitory) {
        throw new Error('Dormitory not found');
      }
      dormitory.registration_status = registration_status;
      await this.dormitoryRepository.save(dormitory);
      return dormitory;
    } catch (error) {
      console.error('Error updating registration status:', error);
      return null;
    }
  }
}
