import { Dormitory } from 'src/dormitory/entities/dormitory.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'informations' })
export class Information {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  description: string;

  @Column()
  image_url: string;

  @Column()
  dormitoryId: number;

  @ManyToOne(() => Dormitory, (dormitory) => dormitory.informations)
  @JoinColumn({ name: 'dormitoryId' })
  dormitory: Dormitory;

  constructor(partial: Partial<Information>) {
    Object.assign(this, partial);
  }
}
