import {
  Controller,
  Get,
  Post,
  Put,
  Delete,
  Body,
  Param,
  Query,
} from '@nestjs/common';
import { InformationService } from './information.service';
import { Information } from './entities/information.entity';
import { StudentAccess, TeacherAccess } from 'src/decorators/guard.decorator';

@Controller('informations')
export class InformationController {
  constructor(private readonly informationService: InformationService) {}

  @TeacherAccess()
  @Post()
  async create(
    @Body() informationData: Partial<Information>,
  ): Promise<Information> {
    return await this.informationService.create(informationData);
  }

  @StudentAccess()
  @TeacherAccess()
  @Get('/dormitory/:dormitory_id')
  async getAll(
    @Param('dormitory_id') dormitory_id: number,
    @Query('page') page: number = 1, // default page is 1
    @Query('keyword') keyword: string = '', // default keyword is empty
  ): Promise<{ information: Information[]; totalPages: number }> {
    return await this.informationService.getAll(dormitory_id, page, keyword);
  }

  @StudentAccess()
  @TeacherAccess()
  @Get(':id')
  async getById(@Param('id') id: number): Promise<Information> {
    return await this.informationService.getById(id);
  }

  @TeacherAccess()
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() informationData: Partial<Information>,
  ): Promise<Information> {
    return await this.informationService.update(id, informationData);
  }

  @TeacherAccess()
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<void> {
    await this.informationService.delete(id);
  }
}
