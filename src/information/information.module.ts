import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Information } from './entities/information.entity';
import { InformationService } from './information.service';
import { InformationController } from './information.controller';

@Module({
  imports: [TypeOrmModule.forFeature([Information])],
  providers: [InformationService],
  controllers: [InformationController],
})
export class InformationModule {}
