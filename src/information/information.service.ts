import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Information } from './entities/information.entity';

@Injectable()
export class InformationService {
  constructor(
    @InjectRepository(Information)
    private readonly informationRepository: Repository<Information>,
  ) {}

  async create(informationData: Partial<Information>): Promise<Information> {
    const newInformation = this.informationRepository.create(informationData);
    return await this.informationRepository.save(newInformation);
  }

  async getAll(
    dormitoryId: number,
    page: number,
    keyword: string,
  ): Promise<{ information: Information[]; totalPages: number }> {
    const limit = 4;
    const offset = (page - 1) * limit;

    const baseQuery = this.informationRepository
      .createQueryBuilder('information')
      .where('information.dormitoryId = :dormitoryId', { dormitoryId });

    if (keyword) {
      baseQuery.andWhere('(information.description LIKE :keyword)', {
        keyword: `%${keyword}%`,
      });
    }

    // Count total records
    const totalRecords = await baseQuery.getCount();

    // Calculate total pages
    const totalPages = Math.ceil(totalRecords / limit);

    // Get the information for the current page
    const information = await baseQuery.skip(offset).take(limit).getMany();

    return { information, totalPages };
  }

  async getById(id: number): Promise<Information> {
    const information = await this.informationRepository.findOneById(id);
    if (!information) {
      throw new NotFoundException(`Information with ID ${id} not found`);
    }
    return information;
  }

  async update(
    id: number,
    informationData: Partial<Information>,
  ): Promise<Information> {
    const information = await this.informationRepository.findOneById(id);
    if (!information) {
      throw new NotFoundException(`Information with ID ${id} not found`);
    }
    Object.assign(information, informationData);
    return await this.informationRepository.save(information);
  }

  async delete(id: number): Promise<void> {
    const information = await this.informationRepository.findOneById(id);
    if (!information) {
      throw new NotFoundException(`Information with ID ${id} not found`);
    }
    await this.informationRepository.delete(id);
  }
}
