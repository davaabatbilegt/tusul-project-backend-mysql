import { Room } from 'src/room/entities/room.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ name: 'items' })
export class Item {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  name: string;

  @Column()
  count: number;

  @ManyToOne(() => Room, (room) => room.items)
  room: Room;

  constructor(partial: Partial<Item>) {
    Object.assign(this, partial);
  }
}
