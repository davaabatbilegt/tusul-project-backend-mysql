import { Dormitory } from 'src/dormitory/entities/dormitory.entity';
import { Student } from 'src/student/entities/student.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'offers' })
export class Offer {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  description: string;

  @Column()
  image_url: string;

  @Column()
  created_at: Date;

  @Column()
  dormitoryId: number;

  @Column()
  studentId: number;

  @ManyToOne(() => Dormitory, (dormitory) => dormitory.offers)
  @JoinColumn({ name: 'dormitoryId' })
  dormitory: Dormitory;

  @ManyToOne(() => Student, (student) => student.offers)
  @JoinColumn({ name: 'studentId' })
  student: Student;

  constructor(partial: Partial<Offer>) {
    Object.assign(this, partial);
  }
}
