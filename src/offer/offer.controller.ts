import {
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Body,
} from '@nestjs/common';
import { OfferService } from './offer.service';
import { Offer } from './entities/offer.entity';
import { StudentAccess, TeacherAccess } from 'src/decorators/guard.decorator';

@Controller('offers')
export class OfferController {
  constructor(private readonly offerService: OfferService) {}

  // @StudentAccess()
  // @TeacherAccess()
  // @Get()
  // async getAllOffers(
  //   @Query('dormitoryId') dormitoryId?: number,
  //   @Query('studentId') studentId?: number,
  // ): Promise<Offer[]> {
  //   return await this.offerService.getAll(studentId, dormitoryId);
  // }

  @StudentAccess()
  @TeacherAccess()
  @Get('/student/:studentId')
  async getAllOffersByStudent(
    @Param('studentId') studentId: number,
  ): Promise<Offer[]> {
    return await this.offerService.getAllByStudent(studentId);
  }

  @StudentAccess()
  @TeacherAccess()
  @Get('/dormitory/:dormitoryId')
  async getAllOffersByDormitory(
    @Param('dormitoryId') dormitoryId: number,
  ): Promise<Offer[]> {
    return await this.offerService.getAllByDormitory(dormitoryId);
  }

  @StudentAccess()
  @TeacherAccess()
  @Get(':id')
  async getOfferById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Offer | undefined> {
    return await this.offerService.getOneById(id);
  }

  @TeacherAccess()
  @Post()
  async createOffer(@Body() offerData: Partial<Offer>): Promise<Offer> {
    return await this.offerService.create(offerData);
  }
}
