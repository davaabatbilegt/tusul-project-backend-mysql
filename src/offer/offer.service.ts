import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Offer } from './entities/offer.entity';

@Injectable()
export class OfferService {
  constructor(
    @InjectRepository(Offer)
    private readonly offerRepository: Repository<Offer>,
  ) {}

  // async getAll(studentId?: number, dormitoryId?: number): Promise<Offer[]> {
  //   const queryBuilder = this.offerRepository.createQueryBuilder('offer');

  //   if (dormitoryId) {
  //     queryBuilder
  //       .leftJoinAndSelect('offer.student', 'student')
  //       .andWhere('offer.dormitoryId = :dormitoryId', { dormitoryId });
  //   }
  //   if (studentId) {
  //     queryBuilder.andWhere('offer.studentId = :studentId', { studentId });
  //   }
  //   return await queryBuilder.getMany();
  // }

  async getAllByDormitory(dormitoryId: number): Promise<Offer[]> {
    const queryBuilder = this.offerRepository.createQueryBuilder('offer');

    if (dormitoryId) {
      queryBuilder
        .leftJoinAndSelect('offer.student', 'student')
        .andWhere('offer.dormitoryId = :dormitoryId', { dormitoryId });
    }
    return await queryBuilder.getMany();
  }

  async getAllByStudent(studentId: number): Promise<Offer[]> {
    const queryBuilder = this.offerRepository.createQueryBuilder('offer');

    if (studentId) {
      queryBuilder
        .leftJoinAndSelect('offer.student', 'student')
        .andWhere('offer.studentId = :studentId', { studentId });
    }
    return await queryBuilder.getMany();
  }

  async getOneById(id: number): Promise<Offer | undefined> {
    return await this.offerRepository.findOneById(id);
  }

  async create(offerData: Partial<Offer>): Promise<Offer> {
    const newOffer = this.offerRepository.create(offerData);
    return await this.offerRepository.save(newOffer);
  }
}
