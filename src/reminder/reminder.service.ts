import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Reminder } from './entities/reminder.entity';

@Injectable()
export class ReminderService {
  constructor(
    @InjectRepository(Reminder)
    private readonly reminderRepository: Repository<Reminder>,
  ) {}

  // async getAll(studentId?: number, teacherId?: number): Promise<Reminder[]> {
  //   const queryBuilder = this.reminderRepository.createQueryBuilder('reminder');

  //   if (studentId) {
  //     queryBuilder.andWhere('reminder.studentId = :studentId', { studentId });
  //   }
  //   if (teacherId) {
  //     queryBuilder
  //       .leftJoinAndSelect('reminder.student', 'student')
  //       .andWhere('reminder.teacherId = :teacherId', { teacherId });
  //   }

  //   return await queryBuilder.getMany();
  // }

  async getAllByTeacher(teacherId: number): Promise<Reminder[]> {
    const queryBuilder = this.reminderRepository.createQueryBuilder('reminder');

    if (teacherId) {
      queryBuilder
        .leftJoinAndSelect('reminder.student', 'student')
        .andWhere('reminder.teacherId = :teacherId', { teacherId });
    }
    return await queryBuilder.getMany();
  }

  async getAllByStudent(studentId: number): Promise<Reminder[]> {
    const queryBuilder = this.reminderRepository.createQueryBuilder('reminder');

    if (studentId) {
      queryBuilder
        .leftJoinAndSelect('reminder.student', 'student')
        .andWhere('reminder.studentId = :studentId', { studentId });
    }
    return await queryBuilder.getMany();
  }

  async getOneById(id: number): Promise<Reminder | undefined> {
    return await this.reminderRepository.findOneById(id);
  }

  async create(reminderData: Partial<Reminder>): Promise<Reminder> {
    const newReminder = this.reminderRepository.create(reminderData);
    return await this.reminderRepository.save(newReminder);
  }
}
