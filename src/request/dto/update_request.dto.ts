import { REQUEST_STATUSES } from 'src/utils/constants';

export class UpdateRequestDto {
  status: REQUEST_STATUSES;
  add_score: number;
  is_gpa: boolean;
}
