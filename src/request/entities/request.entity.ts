import { Dormitory } from 'src/dormitory/entities/dormitory.entity';
import { Student } from 'src/student/entities/student.entity';
import { REQUEST_STATUSES, STATUSES } from 'src/utils/constants';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'requests' })
export class Request {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  phone_number: number;

  @Column()
  email: string;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
  })
  gender: STATUSES;

  @Column()
  gpa: number;

  @Column()
  gpa_desc: string;

  @Column()
  additional_desc: string;

  @Column()
  score: number;

  @Column({
    type: 'enum',
    enum: REQUEST_STATUSES,
    default: REQUEST_STATUSES.SENT,
  })
  status: string;

  @Column()
  dormitoryId: number;

  @Column()
  studentId: number;

  @ManyToOne(() => Dormitory, (dormitory) => dormitory.requests)
  @JoinColumn({ name: 'dormitoryId' })
  dormitory: Dormitory;

  @ManyToOne(() => Student, (student) => student.requests)
  @JoinColumn({ name: 'studentId' })
  student: Student;

  constructor(partial: Partial<Request>) {
    Object.assign(this, partial);
  }
}
