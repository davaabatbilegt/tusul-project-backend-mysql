import {
  Controller,
  Get,
  Param,
  NotFoundException,
  Post,
  Body,
  Put,
  Delete,
  Query,
} from '@nestjs/common';
import { RequestService } from './request.service';
import { Request } from './entities/request.entity';
import { TeacherAccess, StudentAccess } from 'src/decorators/guard.decorator';
import { CreateRequestDto } from './dto/request.dto';
import { UpdateRequestDto } from './dto/update_request.dto';

@Controller('requests')
export class RequestController {
  constructor(private readonly requestService: RequestService) {}

  @TeacherAccess()
  @StudentAccess()
  @Get()
  async findAll(): Promise<Request[]> {
    return await this.requestService.findAllWithStudents();
  }

  // @StudentAccess()
  // @TeacherAccess()
  // @Get('/dormitory/:dormitory_id')
  // async findAllByDormitoryId(
  //   @Param('dormitory_id') dormitory_id: number,
  // ): Promise<Request[]> {
  //   // return this.roomService.findOneByIdWithTeachers(parseInt(id, 10));
  //   return this.requestService.findAllByDormitoryId(dormitory_id);
  // }

  @StudentAccess()
  @TeacherAccess()
  @Get('/dormitory/:dormitory_id')
  async findAllByDormitoryId(
    @Param('dormitory_id') dormitory_id: number,
    @Query('page') page: number = 1, // default page is 1
    @Query('keyword') keyword: string = '', // default keyword is empty
  ): Promise<{ requests: Request[]; totalPages: number }> {
    return this.requestService.findAllByDormitoryId(
      dormitory_id,
      page,
      keyword,
    );
  }

  @TeacherAccess()
  @Get('/calculate/:dormitory_id')
  async calculateRequests(
    @Param('dormitory_id') dormitory_id: number,
  ): Promise<{ requests: Request[]; totalPages: number }> {
    await this.requestService.calculate(dormitory_id);
    return await this.requestService.findAllByDormitoryId(dormitory_id, 1, '');
  }

  @TeacherAccess()
  @Get('/return/:dormitory_id')
  async returnAnswer(
    @Param('dormitory_id') dormitory_id: number,
  ): Promise<{ requests: Request[]; totalPages: number }> {
    await this.requestService.returnAnswer(dormitory_id);
    return await this.requestService.findAllByDormitoryId(dormitory_id, 1, '');
  }

  @TeacherAccess()
  @StudentAccess()
  @Get(':id')
  async findOne(@Param('id') id: string): Promise<Request> {
    const request = await this.requestService.findOneById(Number(id));
    if (!request) {
      throw new NotFoundException(`Request with ID ${id} not found`);
    }
    return request;
  }

  @StudentAccess()
  @Post()
  async create(@Body() createRequestDto: CreateRequestDto): Promise<Request> {
    return this.requestService.create(createRequestDto);
  }

  @StudentAccess()
  @Get(':userId/:dormitoryId')
  async getRequestByUser(
    @Param() params: { userId: number; dormitoryId: number },
  ): Promise<Request> {
    return this.requestService.findByIds(params.userId, params.dormitoryId);
  }

  @TeacherAccess()
  @StudentAccess()
  @Put(':id')
  async update(
    @Param('id') id: number,
    @Body() updateRequestDto: UpdateRequestDto,
  ): Promise<Request> {
    return this.requestService.updateStatusById(id, updateRequestDto);
  }

  @StudentAccess()
  @Delete(':id')
  async delete(@Param('id') id: number): Promise<void> {
    await this.requestService.delete(id);
  }
}
