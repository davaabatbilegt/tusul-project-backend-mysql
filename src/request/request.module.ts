import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RequestService } from './request.service';
import { RequestController } from './request.controller';
import { Request } from './entities/request.entity';
import { StudentModule } from 'src/student/student.module';
import { Affiliated_schoolsModule } from 'src/affiliated_school/affiliated_school.module';
import { DormitoryModule } from 'src/dormitory/dormitory.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([Request]),
    StudentModule,
    Affiliated_schoolsModule,
    DormitoryModule,
  ],
  providers: [RequestService],
  controllers: [RequestController],
  exports: [RequestService],
})
export class RequestModule {}
