import { Dormitory } from 'src/dormitory/entities/dormitory.entity';
import { Item } from 'src/item/entities/item.entity';
import { Student } from 'src/student/entities/student.entity';
import { STATUSES } from 'src/utils/constants';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'rooms' })
export class Room {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column({ type: 'bigint' })
  number: number;

  @Column({
    type: 'enum',
    enum: STATUSES,
    default: STATUSES.ACTIVE,
  })
  gender: STATUSES;

  @Column()
  floor: number;

  @Column()
  student_count: number;

  @Column()
  image_url: string;

  @ManyToOne(() => Dormitory, (dormitory) => dormitory.rooms)
  dormitory: Dormitory;

  @OneToMany(() => Item, (item) => item.room)
  items: Item[];

  @OneToMany(() => Student, (student) => student.room)
  students: Student[];

  constructor(partial: Partial<Room>) {
    Object.assign(this, partial);
  }
}
