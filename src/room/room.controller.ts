import { Controller, Get, Param, Query } from '@nestjs/common';
import { RoomService } from './room.service';
import { Room } from './entities/room.entity';
import { StudentAccess, TeacherAccess } from 'src/decorators/guard.decorator';

@Controller('rooms')
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @StudentAccess()
  @TeacherAccess()
  @Get()
  async findAll(): Promise<Room[]> {
    return this.roomService.findAll();
  }

  // @StudentAccess()
  // @TeacherAccess()
  // @Get('dormitory/:dormitory_id')
  // async findAllByDormitoryId(
  //   @Param('dormitory_id') dormitory_id: number,
  // ): Promise<Room[]> {
  //   // return this.roomService.findOneByIdWithTeachers(parseInt(id, 10));
  //   return this.roomService.findAllByDormitoryId(dormitory_id);
  // }

  @StudentAccess()
  @TeacherAccess()
  @Get('dormitory/:dormitory_id')
  async findAllByDormitoryId(
    @Param('dormitory_id') dormitory_id: number,
    @Query('page') page: number = 1, // default page is 1
    @Query('keyword') keyword: string = '', // default keyword is empty
  ): Promise<{ rooms: Room[]; totalPages: number }> {
    return this.roomService.findAllByDormitoryId(dormitory_id, page, keyword);
  }

  @StudentAccess()
  @TeacherAccess()
  @Get(':id')
  async findOne(@Param('id') id: number): Promise<Room | null> {
    // return this.roomService.findOneByIdWithTeachers(parseInt(id, 10));
    return this.roomService.findOneByIdOne(id);
  }
}
