import { Offer } from 'src/offer/entities/offer.entity';
import { Reminder } from 'src/reminder/entities/reminder.entity';
import { Request } from 'src/request/entities/request.entity';
import { Room } from 'src/room/entities/room.entity';
import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'students' })
export class Student {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column()
  school: string;

  @Column()
  year: number;

  @Column({ type: 'bigint' })
  phone_number: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column()
  microsoft_token: string;

  @Column()
  dormitoryId: number;

  @OneToMany(() => Offer, (offer) => offer.student)
  offers: Offer[];

  @OneToMany(() => Reminder, (reminder) => reminder.student)
  reminders: Reminder[];

  @OneToMany(() => Request, (request) => request.student)
  requests: Request[];

  @ManyToOne(() => Room, (room) => room.students)
  room: Room;

  constructor(partial: Partial<Student>) {
    Object.assign(this, partial);
  }
}
