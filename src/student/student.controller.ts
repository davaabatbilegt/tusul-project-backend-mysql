import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  HttpCode,
  HttpStatus,
} from '@nestjs/common';
import { StudentService } from './student.service';
import { Student } from './entities/student.entity';
import { Public } from 'src/decorators/guard.decorator';

@Controller('students')
export class StudentController {
  constructor(private readonly studentService: StudentService) {}

  @Public()
  @Get()
  async findAll(): Promise<Student[]> {
    return await this.studentService.findAll();
  }

  // @Public()
  // @Get(':id')
  // async findOne(@Param('id') id: string): Promise<Student> {
  //   const student = await this.studentService.findOneById(Number(id));
  //   if (!student) {
  //     throw new NotFoundException(`Student with ID ${id} not found`);
  //   }
  //   return student;
  // }

  @Public()
  @Get(':microsoft_token')
  async findOneWithToken(
    @Param('microsoft_token') microsoft_token: string,
  ): Promise<{ access_token: string } | null> {
    try {
      return await this.studentService.findOneByMicrosoftToken(microsoft_token);
    } catch (error) {
      return null;
    }
  }

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post()
  async createStudent(
    @Body() studentData: Partial<Student>,
  ): Promise<{ access_token: string }> {
    return await this.studentService.createStudent(studentData);
  }
}
