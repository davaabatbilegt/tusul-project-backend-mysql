import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { StudentService } from './student.service';
import { Student } from './entities/student.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StudentController } from './student.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { APP_GUARD } from '@nestjs/core';
import { AuthGuard } from 'src/auth/auth.guard';

@Module({
  imports: [
    ConfigModule,
    TypeOrmModule.forFeature([Student]),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [
    StudentService,
    {
      provide: APP_GUARD,
      useClass: AuthGuard,
    },
  ],
  exports: [StudentService],
  controllers: [StudentController],
})
export class StudentModule {}
