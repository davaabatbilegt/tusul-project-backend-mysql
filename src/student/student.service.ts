import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Student } from './entities/student.entity';
import { USER_TYPES } from 'src/utils/constants';
import { JwtService } from '@nestjs/jwt';
import { passwordMd5Encrypt } from 'src/utils/functions';
import { SignInDto } from 'src/auth/dto/sign-in.dto';
import { UpdateStudentDto } from './dto/update_request.dto';

@Injectable()
export class StudentService {
  constructor(
    @InjectRepository(Student)
    private studentsRepository: Repository<Student>,
    private jwtService: JwtService,
  ) {}

  findAll(): Promise<Student[]> {
    return this.studentsRepository.find();
  }

  findOneById(id: number): Promise<Student | null> {
    return this.studentsRepository.findOneBy({ id });
  }

  findOneByEmail(email: string): Promise<Student | null> {
    return this.studentsRepository.findOneBy({ email });
  }

  async updateStudent(
    id: number,
    updateStudentDto: UpdateStudentDto,
  ): Promise<Student> {
    const student = await this.studentsRepository.findOneById(id);

    if (!student) {
      throw new NotFoundException(`Student with ID ${id} not found`);
    }
    student.dormitoryId = updateStudentDto.dormitoryId;

    // Save the updated student entity
    return this.studentsRepository.save(student);
  }

  async findOneByMicrosoftToken(
    microsoft_token: string,
  ): Promise<{ access_token: string }> {
    try {
      const user = await this.studentsRepository.findOne({
        where: {
          microsoft_token: microsoft_token,
        },
      });
      if (!user) {
        throw new Error('User not found');
      }
      const ex = this.signIn({
        email: user.email,
        password: user.password,
        type: USER_TYPES.STUDENT,
      });
      return {
        access_token: (await ex).access_token,
      };
    } catch (error) {
      throw new Error('Error finding user by Microsoft token: ' + error);
    }
  }

  async createStudent(
    studentData: Partial<Student>,
  ): Promise<{ access_token: string }> {
    studentData.password = await passwordMd5Encrypt(studentData.password);
    const student = this.studentsRepository.create(studentData);
    const user = await this.studentsRepository.save(student);

    const ex = this.signIn({
      email: user.email,
      password: user.password,
      type: USER_TYPES.STUDENT,
    });
    return {
      access_token: (await ex).access_token,
    };
  }

  async signIn(signInDto: SignInDto): Promise<{ access_token: string }> {
    const user = await this.findOneByEmail(signInDto.email);
    if (user?.password !== signInDto.password) {
      throw new UnauthorizedException();
    }
    const payload = {
      id: user.id,
      username: user.email,
      type: signInDto.type,
    };
    return {
      access_token: await this.jwtService.signAsync(payload),
    };
  }
}
