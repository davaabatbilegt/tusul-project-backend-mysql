import { Dormitory } from 'src/dormitory/entities/dormitory.entity';
import { Reminder } from 'src/reminder/entities/reminder.entity';
import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({ name: 'teachers' })
export class Teacher {
  @PrimaryGeneratedColumn({ type: 'bigint' })
  id: number;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column({ unique: true })
  email: string;

  @Column()
  image_url: string;

  @Column({ type: 'bigint' })
  phone_number: number;

  @Column()
  password: string;

  @Column()
  dormitoryId: number;

  @ManyToOne(() => Dormitory, (dormitory) => dormitory.teachers)
  @JoinColumn({ name: 'dormitoryId' })
  dormitory: Dormitory;

  @OneToMany(() => Reminder, (reminder) => reminder.teacher)
  reminders: Reminder[];

  constructor(partial: Partial<Teacher>) {
    Object.assign(this, partial);
  }
}
