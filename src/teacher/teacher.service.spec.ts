import { Test, TestingModule } from '@nestjs/testing';
import { TeacherService } from './teacher.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Teacher } from './entities/teacher.entity';

describe('TeacherService', () => {
  let teacherService: TeacherService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TeacherService,
        {
          provide: getRepositoryToken(Teacher),
          useValue: {
            find: jest.fn(),
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    teacherService = module.get<TeacherService>(TeacherService);
  });

  it('should be defined', () => {
    expect(teacherService).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an array of teachers', async () => {
      const teachers: Teacher[] = [
        {
          id: 1,
          first_name: 'Teacher',
          last_name: 'One',
          email: 'teacher1@example.com',
          image_url: '...',
          phone_number: 123456789,
          password: 'password',
          dormitoryId: 1,
          dormitory: undefined,
          reminders: [],
        },
      ];

      const result = await teacherService.findAll();

      expect(result).toEqual(teachers);
    });
  });

  describe('findOneById', () => {
    it('should return a teacher if found', async () => {
      const teacher: Teacher = {
        id: 1,
        first_name: 'Teacher',
        last_name: 'One',
        email: 'teacher1@example.com',
        image_url: '...',
        phone_number: 123456789,
        password: 'password',
        dormitoryId: 1,
        dormitory: undefined,
        reminders: [],
      };

      const result = await teacherService.findOneById(1);

      expect(result).toEqual(teacher);
    });

    it('should return null if teacher is not found', async () => {
      const result = await teacherService.findOneById(1);

      expect(result).toBeNull();
    });
  });

  describe('findOneByEmail', () => {
    it('should return a teacher if found', async () => {
      const teacher: Teacher = {
        id: 1,
        first_name: 'Teacher',
        last_name: 'One',
        email: 'teacher1@example.com',
        image_url: '...',
        phone_number: 123456789,
        password: 'password',
        dormitoryId: 1,
        dormitory: undefined,
        reminders: [],
      };

      const result = await teacherService.findOneByEmail(
        'teacher1@example.com',
      );

      expect(result).toEqual(teacher);
    });

    it('should return null if teacher is not found', async () => {
      const result = await teacherService.findOneByEmail(
        'nonexistent@example.com',
      );

      expect(result).toBeNull();
    });
  });
});
