import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Teacher } from './entities/teacher.entity';

@Injectable()
export class TeacherService {
  constructor(
    @InjectRepository(Teacher)
    private teacherRepository: Repository<Teacher>,
  ) {}

  findAll(): Promise<Teacher[]> {
    return this.teacherRepository.find();
  }

  findOneById(id: number): Promise<Teacher | null> {
    return this.teacherRepository.findOneBy({ id });
  }

  findOneByEmail(email: string): Promise<Teacher | null> {
    return this.teacherRepository.findOneBy({ email });
  }
}
