export enum STATUSES {
  INACTIVE = 0,
  ACTIVE,
}

export enum REQUEST_STATUSES {
  SENT = 'sent',
  CALCULATE = 'calculate',
  CALCULATED = 'calculated',
  APPROVE = 'approved',
  REFUSE = 'refused',
}

export enum REGISTRATION_STATUSES {
  DEFAULT = 'default',
  CALCULATED = 'calculated',
  RETURNED = 'returned',
}

export enum USER_TYPES {
  ADMIN = 'admin',
  STUDENT = 'student',
  TEACHER = 'teacher',
}

export const IS_PUBLIC_KEY = 'isPublic';
