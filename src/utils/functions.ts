export const passwordMd5Encrypt = async (password: string): Promise<string> => {
  const crypto = await require('crypto');
  return crypto.createHash('md5').update(password).digest('hex');
};
